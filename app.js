"use strict";
const bodyParser = require("body-parser");
const express = require("express");
const logger = require("morgan");
const RouteArgs_1 = require("./code/Route/RouteArgs");
const ClientSocket_1 = require("./routes/Lobby/ClientSocket");
const Index_1 = require("./routes/Lobby/Index");
const ServiceCallsApi_1 = require("./routes/Api/ServiceCallsApi");
class Server {
    static bootstrap() {
        return new Server();
    }
    config() {
        Server.setRoutes(this.app);
        Server.setApiRoutes(this.app);
        this.middleware();
    }
    middleware() {
        this.app.use(logger("dev"));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }
    static setRoutes(app) {
        [
            new Index_1.Index(new RouteArgs_1.RouteArgs(app, "/index")),
            new ClientSocket_1.ClientSocket(new RouteArgs_1.RouteArgs(app, "/client-socket"))
        ].forEach(route => {
            route.setDefaultRoute();
        });
    }
    static setApiRoutes(app) {
        [
            new ServiceCallsApi_1.ServiceCallsApi(new RouteArgs_1.RouteArgs(app))
        ].forEach(api => {
            api.setApiRoutes();
        });
    }
    constructor() {
        this.app = express();
        this.config();
    }
}
Server.bootstrap().app;
