module.exports = function (app, args) {
    var SNSClient = require('aws-snsclient');
    var auth = {
        region: 'eu-west-1'
        , account: '560720505087'
        , topic: args.topic
    };
    var client = SNSClient(auth, function (err, message) {
        console.log("error: ", err);
        console.log("message:", message);
    });

    app.post(args.route, function (req, res) {
        if (req.method === 'POST' && req.url === args.route) {
            console.log(req.headers);
            return client(req, res);
        } else {
            console.log("invalid url: ", req.url);
        }
    });
};
