var aws = require('aws-sdk');
aws.config.loadFromPath('config.json');

var sns = new aws.SNS();

module.exports = function (app, args) {
    var topicArn = "arn:aws:sns:eu-west-1:560720505087:" + args.topic;
    app.post(args.route, function (req, res) {
        var message = req.body.Message;
        var subject = req.body.Subject;
        if (message && subject) {
            publish(subject, message, function (d) {
                console.log('data from publish: ', d);
                res.sendStatus(200).json();
            });
        } else {
            throw new Error("insufficient post parameters");
        }
    });

    function publish(subject, message, doneback) {
        var params = {
            Message: message, /* required */
            MessageAttributes: {
                someKey: {
                    DataType: 'String', /* required */
                    //BinaryValue: new Buffer('...') || 'STRING_VALUE',
                    StringValue: 'someKey-Value'
                }
                /* anotherKey: ... */
            },
            MessageStructure: 'string',
            // PhoneNumber: 'STRING_VALUE',
            Subject: subject,
            TopicArn: topicArn
        };
        sns.publish(params, function (err, data) {
            if (err) {
                console.log(err, err.stack);
            } else {
                doneback(data);
            }
        });
    }
};
