import { EmailEngine } from "../code/Engines/EMailEngine"
var util = require('util');
module.exports = function (app, args) {
    app.post(args.route, function (req, res) {
        let email = req.body.EMail;
        let password = req.body.Password;
        let count = req.body.Count || 10;

        let emaile = new EmailEngine(email, password);
        emaile.watch(count, (i, email) => {
            console.log(i, "indexed mail watched");
        }, (mails) => {
            console.log("***********watch completed*************");
            res.sendStatus(200).json();
        });
    });
};