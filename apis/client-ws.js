module.exports = function (app, args) {
    var expressws = require('express-ws')(app);
    var util = require('util');
    app.post(args.route, function (req, res) {
        var wss = expressws.getWss();
        var i = 0;
        wss.clients.forEach(function each(client) {
            if (client.readyState === WebSocket.OPEN) {
                client.send("afaki");
                if (++i == wss.clients.length) {
                    res.send("OK");
                }
            }
        });
        if (wss.clients.length == 0) {
            res.sendStatus(200).json();
        }
    });

    app.ws(args.route, function(ws, req) {
        ws.on('message', function(msg) {
            ws.send(msg);
        });
        ws.on('close', function(reasonCode, description) {
            console.log(reasonCode, description);
        });
    });
};
