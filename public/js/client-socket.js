$(function() {

    var ws = new WebSocket('ws://localhost:8090/client-ws', 'echo-protocol');

    ws.addEventListener("message", function(e) {
        var msg = "<span>" + e.data + "</span>";
        $('#message').html($('#message').html() + '<br>' + msg);
    });

});