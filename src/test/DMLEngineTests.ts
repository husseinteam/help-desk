/// <reference path="../../_all.d.ts" />
"use strict";
import {DBArgs} from "../code/DB/DBArgs";
import {DMLEngine} from "../code/DB/DMLEngine";
import {SelectKey} from "../code/DB/SelectKey";

import {DDLEngine} from "../code/DB/DDLEngine";
import {EUpsert} from "../code/DB/EUpsert";
import {TestModel} from "../code/Model/TestModel";

import { suite, test, slow, timeout, skip, only } from "mocha-typescript";
import {should} from  "chai";
should();

@suite class DMLTests {
    private ddle: DDLEngine;
    private target: DMLEngine;

    before() {
        this.ddle = new DDLEngine(DBArgs.destekV1);
        this.target = new DMLEngine(DBArgs.destekV1);
    }

    @timeout(5000)
    @test "should add new table"(done: () => void) {
        this.ddle.newTableStrict({name: "id", type: "INT"}, new TestModel()).then(res => done());
    }

    @test async "should insert new item"() {
        let r = await this.target.insert(new TestModel({name: "item1", type: "generic"}));
        r.id.should.not.eq(0, "id should not have been 0");
        r.inserted.name.should.eq("item1", `entity.name should have been identical to 'item1' but it is ${r.inserted.name}`);
    }
    @test async "should update existing item"() {
        let r = await this.target.insert(new TestModel({name: "item2", type: "plastic"}));
        r.id.should.not.eq(0, "id should not have been 0");
        r.inserted.name.should.eq("item2", `entity.name should have been identical to 'item2' but it is ${r.inserted.name}`);
        let r2 = await this.target.update(new SelectKey("id", "=", r.id), new TestModel({name: "item2", type: "electronic"}));
        r2.changed.should.not.eq(0, "id should not have been 0");
        r2.updated.type.should.eq("electronic", `entity.type should have been identical to 'electronic' but it is ${r2.updated.type}`);
    }
    @test async "should upsert an item"() {
        let r = await this.target.insert(new TestModel({name: "item3", type: "plastic"}));
        r.id.should.not.eq(0, "id should not have been 0");
        r.inserted.name.should.eq("item3", `entity.name should have been identical to 'item3' but it is ${r.inserted.name}`);
        let r2 = await this.target.upsert(new SelectKey("id", "=", r.id), new TestModel({name: "item4", type: "laundry"}));
        r2.result.should.not.eq(0, "upsert result should not have been 0");
        r2.upserted.type.should.eq("laundry", `entity.type should have been identical to 'laundry' but it is ${r2.upserted.type}`);
        r2.type.should.eq(EUpsert.Update, "item4 should have been updated not reinserted");
        let r3 = await this.target.upsert(new SelectKey("type", "=", "forensics"), new TestModel({name: "item5", type: "forensics"}));
        r3.result.should.not.eq(0, "upsert result should not have been 0");
        r3.upserted.type.should.eq("forensics", `entity.type should have been identical to 'forensics' but it is ${r3.upserted.type}`);
        r3.type.should.eq(EUpsert.Insert, "item5 should have been inserted not reupdated");
    }
    @test async "should select one or more items"() {
        let models = await this.target.select(new TestModel(), [new SelectKey("id", ">", 0)]);
        models.size().should.at.least(1, `models.size is ${models.size()}`);
    }
    @test async "should delete one item"() {
        let models = await this.target.select<TestModel>(new TestModel(), [new SelectKey("id", ">", 0)]);
        models.size().should.at.least(1, `models.size is ${models.size()}`);
        let r = await this.target.delete(new TestModel(), [new SelectKey("id", "=", models.get(0).id)]);
        r.affected.should.eq(1, "affected should have been 1");
    }

}
