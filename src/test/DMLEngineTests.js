/// <reference path="../../_all.d.ts" />
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const DBArgs_1 = require("../code/DB/DBArgs");
const DMLEngine_1 = require("../code/DB/DMLEngine");
const SelectKey_1 = require("../code/DB/SelectKey");
const DDLEngine_1 = require("../code/DB/DDLEngine");
const EUpsert_1 = require("../code/DB/EUpsert");
const TestModel_1 = require("../code/Model/TestModel");
const mocha_typescript_1 = require("mocha-typescript");
const chai_1 = require("chai");
chai_1.should();
let DMLTests = class DMLTests {
    before() {
        this.ddle = new DDLEngine_1.DDLEngine(DBArgs_1.DBArgs.destekV1);
        this.target = new DMLEngine_1.DMLEngine(DBArgs_1.DBArgs.destekV1);
    }
    "should add new table"(done) {
        this.ddle.newTableStrict({ name: "id", type: "INT" }, new TestModel_1.TestModel()).then(res => done());
    }
    "should insert new item"() {
        return __awaiter(this, void 0, void 0, function* () {
            let r = yield this.target.insert(new TestModel_1.TestModel({ name: "item1", type: "generic" }));
            r.id.should.not.eq(0, "id should not have been 0");
            r.inserted.name.should.eq("item1", `entity.name should have been identical to 'item1' but it is ${r.inserted.name}`);
        });
    }
    "should update existing item"() {
        return __awaiter(this, void 0, void 0, function* () {
            let r = yield this.target.insert(new TestModel_1.TestModel({ name: "item2", type: "plastic" }));
            r.id.should.not.eq(0, "id should not have been 0");
            r.inserted.name.should.eq("item2", `entity.name should have been identical to 'item2' but it is ${r.inserted.name}`);
            let r2 = yield this.target.update(new SelectKey_1.SelectKey("id", "=", r.id), new TestModel_1.TestModel({ name: "item2", type: "electronic" }));
            r2.changed.should.not.eq(0, "id should not have been 0");
            r2.updated.type.should.eq("electronic", `entity.type should have been identical to 'electronic' but it is ${r2.updated.type}`);
        });
    }
    "should upsert an item"() {
        return __awaiter(this, void 0, void 0, function* () {
            let r = yield this.target.insert(new TestModel_1.TestModel({ name: "item3", type: "plastic" }));
            r.id.should.not.eq(0, "id should not have been 0");
            r.inserted.name.should.eq("item3", `entity.name should have been identical to 'item3' but it is ${r.inserted.name}`);
            let r2 = yield this.target.upsert(new SelectKey_1.SelectKey("id", "=", r.id), new TestModel_1.TestModel({ name: "item4", type: "laundry" }));
            r2.result.should.not.eq(0, "upsert result should not have been 0");
            r2.upserted.type.should.eq("laundry", `entity.type should have been identical to 'laundry' but it is ${r2.upserted.type}`);
            r2.type.should.eq(EUpsert_1.EUpsert.Update, "item4 should have been updated not reinserted");
            let r3 = yield this.target.upsert(new SelectKey_1.SelectKey("type", "=", "forensics"), new TestModel_1.TestModel({ name: "item5", type: "forensics" }));
            r3.result.should.not.eq(0, "upsert result should not have been 0");
            r3.upserted.type.should.eq("forensics", `entity.type should have been identical to 'forensics' but it is ${r3.upserted.type}`);
            r3.type.should.eq(EUpsert_1.EUpsert.Insert, "item5 should have been inserted not reupdated");
        });
    }
    "should select one or more items"() {
        return __awaiter(this, void 0, void 0, function* () {
            let models = yield this.target.select(new TestModel_1.TestModel(), [new SelectKey_1.SelectKey("id", ">", 0)]);
            models.size().should.at.least(1, `models.size is ${models.size()}`);
        });
    }
    "should delete one item"() {
        return __awaiter(this, void 0, void 0, function* () {
            let models = yield this.target.select(new TestModel_1.TestModel(), [new SelectKey_1.SelectKey("id", ">", 0)]);
            models.size().should.at.least(1, `models.size is ${models.size()}`);
            let r = yield this.target.delete(new TestModel_1.TestModel(), [new SelectKey_1.SelectKey("id", "=", models.get(0).id)]);
            r.affected.should.eq(1, "affected should have been 1");
        });
    }
};
__decorate([
    mocha_typescript_1.timeout(5000),
    mocha_typescript_1.test,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Function]),
    __metadata("design:returntype", void 0)
], DMLTests.prototype, "should add new table", null);
__decorate([
    mocha_typescript_1.test,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DMLTests.prototype, "should insert new item", null);
__decorate([
    mocha_typescript_1.test,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DMLTests.prototype, "should update existing item", null);
__decorate([
    mocha_typescript_1.test,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DMLTests.prototype, "should upsert an item", null);
__decorate([
    mocha_typescript_1.test,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DMLTests.prototype, "should select one or more items", null);
__decorate([
    mocha_typescript_1.test,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DMLTests.prototype, "should delete one item", null);
DMLTests = __decorate([
    mocha_typescript_1.suite
], DMLTests);
