/// <reference path="../../_all.d.ts" />
"use strict";
import {DBArgs} from "../code/DB/DBArgs";
import {TableBuilderService} from "../code/DB/TableBuilderService";


import { suite, test, slow, timeout, skip, only } from "mocha-typescript";

@suite class TableBuilder {
    private target: TableBuilderService;

    before() {
        this.target = new TableBuilderService(DBArgs.destekV1);
    }

    @test async "should build all tables"() {
        await this.target.buildAll(true).catch(err => { console.log(`failed; reason is: ${err}`); throw(err); });
    }
}
