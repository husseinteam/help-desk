/// <reference path="../../_all.d.ts" />
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const DBArgs_1 = require("../code/DB/DBArgs");
const TableBuilderService_1 = require("../code/DB/TableBuilderService");
const mocha_typescript_1 = require("mocha-typescript");
let TableBuilder = class TableBuilder {
    before() {
        this.target = new TableBuilderService_1.TableBuilderService(DBArgs_1.DBArgs.destekV1);
    }
    "should build all tables"() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.target.buildAll(true).catch(err => { console.log(`failed; reason is: ${err}`); throw (err); });
        });
    }
};
__decorate([
    mocha_typescript_1.test,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], TableBuilder.prototype, "should build all tables", null);
TableBuilder = __decorate([
    mocha_typescript_1.suite
], TableBuilder);
