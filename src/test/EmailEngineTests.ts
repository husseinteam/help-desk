/// <reference path="../../_all.d.ts" />
"use strict";


import {EmailEngine} from "../code/Engines/EMailEngine";
import { suite, test, slow, timeout, skip, only } from "mocha-typescript";

@suite class EmailEngineTest {
    private target: EmailEngine;

    before() {
        this.target = new EmailEngine("destek@vartek.com.tr", "destek_q1w2e3");
    }

    @timeout(20000)
    @test "should list all mails"(done: () => void) {
        this.target.list(10, rlist => {
            console.log(`emails-listing: ${rlist.index}) from: ${rlist.email.fromMail}`);
            if (rlist.last) { done(); }
        });
    }
}
