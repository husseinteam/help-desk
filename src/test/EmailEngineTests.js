/// <reference path="../../_all.d.ts" />
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const EMailEngine_1 = require("../code/Engines/EMailEngine");
const mocha_typescript_1 = require("mocha-typescript");
let EmailEngineTest = class EmailEngineTest {
    before() {
        this.target = new EMailEngine_1.EmailEngine("destek@vartek.com.tr", "destek_q1w2e3");
    }
    "should list all mails"(done) {
        this.target.list(10, rlist => {
            console.log(`emails-listing: ${rlist.index}) from: ${rlist.email.fromMail}`);
            if (rlist.last) {
                done();
            }
        });
    }
};
__decorate([
    mocha_typescript_1.timeout(20000),
    mocha_typescript_1.test,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Function]),
    __metadata("design:returntype", void 0)
], EmailEngineTest.prototype, "should list all mails", null);
EmailEngineTest = __decorate([
    mocha_typescript_1.suite
], EmailEngineTest);
