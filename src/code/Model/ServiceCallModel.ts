/// <reference path="../../../_all.d.ts" />
"use strict";
import {BaseModel} from "./Base/BaseModel";
import {CustomerModel} from "./CustomerModel";
import {StaffModel} from "./StaffModel";
import {IModel} from "./Base/IModel";
import {List} from "../Collections/List";
import {EServiceCallStatus} from "./Enums/EServiceCallStatus";
import {IEntityModel} from "./Base/IEntityModel";

export class ServiceCallModel extends BaseModel<ServiceCallModel> implements IModel<ServiceCallModel> {

    public serviceNumber: string;
    public subject: string;
    public status: EServiceCallStatus;
    public content: string;
    public generatedOn: Date;
    public callerCustomer: CustomerModel;
    public dialingStaff: StaffModel;
    public closerStaff: StaffModel;

    constructor(row?: any) {
        super(row);
        if (row) {
            this.id = parseInt(row.id);
            this.serviceNumber = row.serviceNumber.toString();
            this.subject = row.subject.toString();
            this.status = EServiceCallStatus.parse(row.status.toString());
            this.content = row.content.toString();
            this.generatedOn = new Date(row.generatedOn.toString());
        };
    }

    public tableName() : string {
        return "service_calls";
    }

    public tableFields() : [{ name: string, type: string }] {
        return [
            { name: "serviceNumber", type: "varchar(256)" },
            { name: "subject", type: "varchar(256)" },
            { name: "status", type: "ENUM('Open', 'Processing', 'Closed')" },
            { name: "content", type: "longtext" },
            { name: "generatedOn", type: "datetime" }
        ];
    }

    public referencingFields() : List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }> {
        return new List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }>([
            { name: "callerCustomerId", type: "int", local: this.callerCustomer, references: new CustomerModel() },
            { name: "dialingStaffId", type: "int", local: this.dialingStaff, references: new StaffModel() },
            { name: "closerStaffId", type: "int", local: this.closerStaff, references: new StaffModel() }
        ]);
    }

    public  generator() : (...args: any[]) => ServiceCallModel {
        return (...args: any[]) => {
          return new ServiceCallModel();
        };
    }

}
