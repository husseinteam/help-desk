/// <reference path="../../../../_all.d.ts" />
"use strict";
export enum EServiceCallStatus {
    Open,
    Processing,
    Closed
}

export module EServiceCallStatus {
    export function toString(status: EServiceCallStatus) {
        return EServiceCallStatus[status];
    }

    export function parse(status: string) {
        return EServiceCallStatus[status];
    }
}
