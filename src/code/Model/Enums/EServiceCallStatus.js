/// <reference path="../../../../_all.d.ts" />
"use strict";
var EServiceCallStatus;
(function (EServiceCallStatus) {
    EServiceCallStatus[EServiceCallStatus["Open"] = 0] = "Open";
    EServiceCallStatus[EServiceCallStatus["Processing"] = 1] = "Processing";
    EServiceCallStatus[EServiceCallStatus["Closed"] = 2] = "Closed";
})(EServiceCallStatus = exports.EServiceCallStatus || (exports.EServiceCallStatus = {}));
(function (EServiceCallStatus) {
    function toString(status) {
        return EServiceCallStatus[status];
    }
    EServiceCallStatus.toString = toString;
    function parse(status) {
        return EServiceCallStatus[status];
    }
    EServiceCallStatus.parse = parse;
})(EServiceCallStatus = exports.EServiceCallStatus || (exports.EServiceCallStatus = {}));
