/// <reference path="../../../_all.d.ts" />
"use strict";
import {BaseModel} from "./Base/BaseModel";
import {IModel} from "./Base/IModel";
import {List} from "../Collections/List";
import {IEntityModel} from "./Base/IEntityModel";

export class TestModel extends BaseModel<TestModel> implements IModel<TestModel> {

    public name: string;
    public type: string;

    constructor(row?: any) {
        super(row);
        if (row) {
            this.id = parseInt(row.id) || 0;
            this.name = row.name.toString();
            this.type = row.type.toString();
        }
    }

    tableFields(): [{ name: string; type: string }] {
        return [
            {name: "name", type: "varchar(256)"},
            {name: "type", type: "varchar(256)"}
        ];
    }

    public  generator(): (...args: any[]) => TestModel {
        return (...args: any[]) => {
            return new TestModel(args[0]);
        };
    }

    tableName(): string {
        return "test";
    }

    public referencingFields() : List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }> {
        return new List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }>();
    }
}