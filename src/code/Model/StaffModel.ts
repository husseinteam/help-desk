/// <reference path="../../../_all.d.ts" />
"use strict";
import {BaseModel} from "./Base/BaseModel";
import {IModel} from "./Base/IModel";
import {List} from "../Collections/List";
import {IEntityModel} from "./Base/IEntityModel";

export class StaffModel extends BaseModel<StaffModel> implements IModel<StaffModel> {

    public firstName: string;
    public lastName: string;
    public email: string;

    constructor(row?: any) {
        super(row);
        if (row) {
            this.id = parseInt(row.id);
            this.firstName = row.firstName.toString();
            this.lastName = row.firstName.toString();
            this.email = row.email.toString();
        }
    }

    public tableName() : string {
        return "staff";
    }

    public tableFields() : [{ name: string, type: string }] {
        return [
            { name: "firstName", type: "varchar(256)" },
            { name: "lastName", type: "varchar(256)" },
            { name: "email", type: "varchar(256)" }
        ];
    }

    public  generator() : (...args: any[]) => StaffModel {
        return (...args: any[]) => {
            return new StaffModel();
        };
    }

    public referencingFields() : List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }> {
        return new List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }>();
    }
}
