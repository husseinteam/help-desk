/// <reference path="../../../_all.d.ts" />
"use strict";
import {BaseModel} from "./Base/BaseModel";
import {IModel} from "./Base/IModel";
import {List} from "../Collections/List";
import {IEntityModel} from "./Base/IEntityModel";

export class CompanyModel extends BaseModel<CompanyModel> implements IModel<CompanyModel> {

    public title: string;
    public logo: string;

    constructor(row?: any) {
        super(row);
        if (row) {
            this.id = parseInt(row.id);
            this.title = row.title.toString();
            this.logo = row.logo.toString();
        }
    }

    public tableName() : string {
        return "companies";
    }

    public tableFields() : [{ name: string, type: string }] {
        return [
            { name: "title", type: "varchar(256)" },
            { name: "logo", type: "longtext" }
        ];
    }

    public  generator() : (...args: any[]) => CompanyModel {
        return (...args: any[]) => {
            return new CompanyModel();
        };
    }
    public referencingFields() : List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }> {
        return new List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }>();
    }
}
