/// <reference path="../../../_all.d.ts" />
"use strict";
import {BaseModel} from "./Base/BaseModel";
import {IModel} from "./Base/IModel";
import {List} from "../Collections/List";
import {IEntityModel} from "./Base/IEntityModel";
import {CompanyModel} from "./CompanyModel";

export class CustomerModel extends BaseModel<CustomerModel> implements IModel<CustomerModel> {

    public firstName: string;
    public lastName: string;
    public email: string;
    public company: CompanyModel;

    constructor(row?: any) {
        super(row);
        if (row) {
            this.id = parseInt(row.id);
            this.firstName = row.firstName.toString();
            this.lastName = row.firstName.toString();
            this.email = row.email.toString();
        }
    }

    public tableName() : string {
        return "customers";
    }

    public tableFields() : [{ name: string, type: string }] {
        return [
            { name: "firstName", type: "varchar(256)" },
            { name: "lastName", type: "varchar(256)" },
            { name: "email", type: "varchar(256)" }
        ];
    }

    public  generator() : (...args: any[]) => CustomerModel {
        return (...args: any[]) => {
            return new CustomerModel();
        };
    }
    public referencingFields() : List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }> {
        return new List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }>([
            { name: "companyId", type: "int", local: this.company, references: new CompanyModel() },
        ]);
    }
}
