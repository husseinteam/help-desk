/// <reference path="../../../../_all.d.ts" />
"use strict";
import {List} from "../../Collections/List";
import {IModel} from "./IModel";
import {DMLEngine} from "../../DB/DMLEngine";
import {SelectKey} from "../../DB/SelectKey";
import {DBArgs} from "../../DB/DBArgs";
import {IEntityModel} from "./IEntityModel";

export abstract class BaseModel<T extends IModel<T>> {

    public id: number;

    constructor(row?: any) {
        if (row) {
            let dmle = new DMLEngine(DBArgs.destekV1);
            for (let ref of this.referencingFields()) {
                dmle.select<T>(ref.references as T, [new SelectKey("id", "=", row[ref.name])]).then((models) => {
                    ref.local = models.get(0) as IEntityModel;
                });
            }
        }
    }

    protected abstract referencingFields(): List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }>;

    public parseSetPairs() {
        let constraintString = "";
        let d = this;
        for (let prop in d) {
            if (d.hasOwnProperty(prop) && prop !== "id") {
                constraintString += ` ${prop}=?, `;
            }
        }
        constraintString = constraintString.slice(0, -2);
        return constraintString;
    }

    public toKeyValuePairs(): List<{ key: string, value: string }> {
        let pairs = new List<{ key: string, value: string }>();
        let d = this;
        for (let prop in d) {
            if (d.hasOwnProperty(prop) && prop !== "id") {
                pairs.add({key: prop, value: d[prop].toString()});
            }
        }
        return pairs;
    }

}