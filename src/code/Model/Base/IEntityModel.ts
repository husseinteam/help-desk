/// <reference path="../../../../_all.d.ts" />
"use strict";
import {List} from "../../Collections/List";

export interface IEntityModel {

    tableName(): string;
    tableFields() : [{ name: string, type: string }];
    referencingFields() : List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }>;

}