/// <reference path="../../../../_all.d.ts" />
"use strict";
import {List} from "../../Collections/List";
import {IEntityModel} from "./IEntityModel";

export interface IModel<T extends IModel<T>> extends IEntityModel {

    parseSetPairs();

    generator() : (...args: any[]) => T;
    toKeyValuePairs() : List<{ key: string, value: string }>;

}