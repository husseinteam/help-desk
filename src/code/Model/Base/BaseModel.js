/// <reference path="../../../../_all.d.ts" />
"use strict";
const List_1 = require("../../Collections/List");
const DMLEngine_1 = require("../../DB/DMLEngine");
const SelectKey_1 = require("../../DB/SelectKey");
const DBArgs_1 = require("../../DB/DBArgs");
class BaseModel {
    constructor(row) {
        if (row) {
            let dmle = new DMLEngine_1.DMLEngine(DBArgs_1.DBArgs.destekV1);
            for (let ref of this.referencingFields()) {
                dmle.select(ref.references, [new SelectKey_1.SelectKey("id", "=", row[ref.name])]).then((models) => {
                    ref.local = models.get(0);
                });
            }
        }
    }
    parseSetPairs() {
        let constraintString = "";
        let d = this;
        for (let prop in d) {
            if (d.hasOwnProperty(prop) && prop !== "id") {
                constraintString += ` ${prop}=?, `;
            }
        }
        constraintString = constraintString.slice(0, -2);
        return constraintString;
    }
    toKeyValuePairs() {
        let pairs = new List_1.List();
        let d = this;
        for (let prop in d) {
            if (d.hasOwnProperty(prop) && prop !== "id") {
                pairs.add({ key: prop, value: d[prop].toString() });
            }
        }
        return pairs;
    }
}
exports.BaseModel = BaseModel;
