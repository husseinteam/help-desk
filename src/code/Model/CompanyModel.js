/// <reference path="../../../_all.d.ts" />
"use strict";
const BaseModel_1 = require("./Base/BaseModel");
const List_1 = require("../Collections/List");
class CompanyModel extends BaseModel_1.BaseModel {
    constructor(row) {
        super(row);
        if (row) {
            this.id = parseInt(row.id);
            this.title = row.title.toString();
            this.logo = row.logo.toString();
        }
    }
    tableName() {
        return "companies";
    }
    tableFields() {
        return [
            { name: "title", type: "varchar(256)" },
            { name: "logo", type: "longtext" }
        ];
    }
    generator() {
        return (...args) => {
            return new CompanyModel();
        };
    }
    referencingFields() {
        return new List_1.List();
    }
}
exports.CompanyModel = CompanyModel;
