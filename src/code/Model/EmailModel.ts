/// <reference path="../../../_all.d.ts" />
"use strict";
import {BaseModel} from "./Base/BaseModel";
import {IModel} from "./Base/IModel";
import {List} from "../Collections/List";
import {IEntityModel} from "./Base/IEntityModel";

export class EmailModel extends BaseModel<EmailModel> implements IModel<EmailModel> {

    public itemId: string;
    public internetMessageId: string;
    public fromName: string;
    public fromMail: string;
    public sentOn: Date;
    public generatedOn: Date;
    public displayTo: string;
    public content: string;

    constructor(row?: any) {
        super(row);
        if (row) {
            this.itemId = row.Id.UniqueId.toString();
            this.internetMessageId = row.InternetMessageId.toString();
            this.fromName = row.From.name.toString();
            this.fromMail = row.From.address.toString();
            this.sentOn = row.DateTimeSent.momentDate.toDate();
            this.generatedOn = row.DateTimeCreated.momentDate.toDate();
            this.displayTo = row.DisplayTo.toString();
            this.content = row.Body.text.toString();
        }
    }

    public tableName() : string {
        return "emailseq";
    }

    public tableFields() : [{ name: string, type: string }] {
        return [
            { name: "itemId", type: "varchar(256)" },
            { name: "internetMessageId", type: "varchar(256)" },
            { name: "fromName", type: "varchar(256)" },
            { name: "fromMail", type: "varchar(256)" },
            { name: "sentOn", type: "datetime" },
            { name: "generatedOn", type: "datetime" },
            { name: "displayTo", type: "varchar(256)" },
            { name: "content", type: "longtext" }
        ];
    }

    public  generator() : (...args: any[]) => EmailModel {
        return (...args: any[]) => {
          return new EmailModel(args[0]);
        };
    }
    public referencingFields() : List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }> {
        return new List<{ name: string, type: string, local: IEntityModel, references: IEntityModel }>();
    }
}
