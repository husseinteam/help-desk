/// <reference path="../../../_all.d.ts" />
"use strict";
const BaseModel_1 = require("./Base/BaseModel");
const List_1 = require("../Collections/List");
class StaffModel extends BaseModel_1.BaseModel {
    constructor(row) {
        super(row);
        if (row) {
            this.id = parseInt(row.id);
            this.firstName = row.firstName.toString();
            this.lastName = row.firstName.toString();
            this.email = row.email.toString();
        }
    }
    tableName() {
        return "staff";
    }
    tableFields() {
        return [
            { name: "firstName", type: "varchar(256)" },
            { name: "lastName", type: "varchar(256)" },
            { name: "email", type: "varchar(256)" }
        ];
    }
    generator() {
        return (...args) => {
            return new StaffModel();
        };
    }
    referencingFields() {
        return new List_1.List();
    }
}
exports.StaffModel = StaffModel;
