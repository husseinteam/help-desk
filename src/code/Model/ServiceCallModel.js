/// <reference path="../../../_all.d.ts" />
"use strict";
const BaseModel_1 = require("./Base/BaseModel");
const CustomerModel_1 = require("./CustomerModel");
const StaffModel_1 = require("./StaffModel");
const List_1 = require("../Collections/List");
const EServiceCallStatus_1 = require("./Enums/EServiceCallStatus");
class ServiceCallModel extends BaseModel_1.BaseModel {
    constructor(row) {
        super(row);
        if (row) {
            this.id = parseInt(row.id);
            this.serviceNumber = row.serviceNumber.toString();
            this.subject = row.subject.toString();
            this.status = EServiceCallStatus_1.EServiceCallStatus.parse(row.status.toString());
            this.content = row.content.toString();
            this.generatedOn = new Date(row.generatedOn.toString());
        }
        ;
    }
    tableName() {
        return "service_calls";
    }
    tableFields() {
        return [
            { name: "serviceNumber", type: "varchar(256)" },
            { name: "subject", type: "varchar(256)" },
            { name: "status", type: "ENUM('Open', 'Processing', 'Closed')" },
            { name: "content", type: "longtext" },
            { name: "generatedOn", type: "datetime" }
        ];
    }
    referencingFields() {
        return new List_1.List([
            { name: "callerCustomerId", type: "int", local: this.callerCustomer, references: new CustomerModel_1.CustomerModel() },
            { name: "dialingStaffId", type: "int", local: this.dialingStaff, references: new StaffModel_1.StaffModel() },
            { name: "closerStaffId", type: "int", local: this.closerStaff, references: new StaffModel_1.StaffModel() }
        ]);
    }
    generator() {
        return (...args) => {
            return new ServiceCallModel();
        };
    }
}
exports.ServiceCallModel = ServiceCallModel;
