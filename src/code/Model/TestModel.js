/// <reference path="../../../_all.d.ts" />
"use strict";
const BaseModel_1 = require("./Base/BaseModel");
const List_1 = require("../Collections/List");
class TestModel extends BaseModel_1.BaseModel {
    constructor(row) {
        super(row);
        if (row) {
            this.id = parseInt(row.id) || 0;
            this.name = row.name.toString();
            this.type = row.type.toString();
        }
    }
    tableFields() {
        return [
            { name: "name", type: "varchar(256)" },
            { name: "type", type: "varchar(256)" }
        ];
    }
    generator() {
        return (...args) => {
            return new TestModel(args[0]);
        };
    }
    tableName() {
        return "test";
    }
    referencingFields() {
        return new List_1.List();
    }
}
exports.TestModel = TestModel;
