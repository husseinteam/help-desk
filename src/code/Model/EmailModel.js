/// <reference path="../../../_all.d.ts" />
"use strict";
const BaseModel_1 = require("./Base/BaseModel");
const List_1 = require("../Collections/List");
class EmailModel extends BaseModel_1.BaseModel {
    constructor(row) {
        super(row);
        if (row) {
            this.itemId = row.Id.UniqueId.toString();
            this.internetMessageId = row.InternetMessageId.toString();
            this.fromName = row.From.name.toString();
            this.fromMail = row.From.address.toString();
            this.sentOn = row.DateTimeSent.momentDate.toDate();
            this.generatedOn = row.DateTimeCreated.momentDate.toDate();
            this.displayTo = row.DisplayTo.toString();
            this.content = row.Body.text.toString();
        }
    }
    tableName() {
        return "emailseq";
    }
    tableFields() {
        return [
            { name: "itemId", type: "varchar(256)" },
            { name: "internetMessageId", type: "varchar(256)" },
            { name: "fromName", type: "varchar(256)" },
            { name: "fromMail", type: "varchar(256)" },
            { name: "sentOn", type: "datetime" },
            { name: "generatedOn", type: "datetime" },
            { name: "displayTo", type: "varchar(256)" },
            { name: "content", type: "longtext" }
        ];
    }
    generator() {
        return (...args) => {
            return new EmailModel(args[0]);
        };
    }
    referencingFields() {
        return new List_1.List();
    }
}
exports.EmailModel = EmailModel;
