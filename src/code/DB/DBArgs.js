/// <reference path="../../../_all.d.ts" />
"use strict";
class DBArgs {
    constructor(_host, _database, _user, _password) {
        this._host = _host;
        this._database = _database;
        this._user = _user;
        this._password = _password;
    }
    get host() {
        return this._host;
    }
    get database() {
        return this._database;
    }
    get user() {
        return this._user;
    }
    get password() {
        return this._password;
    }
    static get destekV1() {
        return new DBArgs("localhost", "destekdb", "destekadmin", "vartek_q1w2e3");
    }
}
exports.DBArgs = DBArgs;
