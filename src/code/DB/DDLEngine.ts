/// <reference path="../../../_all.d.ts" />
"use strict";
import {DBEngine} from "./DBEngine";
import {DBArgs} from "./DBArgs";
import {IEntityModel} from "../Model/Base/IEntityModel";

export class DDLEngine extends DBEngine {
    connection: any;

    constructor(dbParams: DBArgs) {
        super();
        // get the client
        let mysql = require("mysql2");
        // create the connection
        this.connection = mysql.createConnection({
            host: dbParams.host,
            database: dbParams.database,
            user: dbParams.user,
            password: dbParams.password
        });
    }

    static reduceKeys(fields: any) {
        let fieldsString = "";
        for (let {name, type} of fields) {
            fieldsString += `, \`${name}\` ${type} NOT NULL`;
        }
        return fieldsString;
    }

    newTable(pk: { name: string, type: string }, table: IEntityModel): Promise<{ result: any }> {
        return new Promise<{ result: any }>((resolve, reject) => {
            let fieldsString = DDLEngine.reduceKeys(table.tableFields()) + DDLEngine.reduceKeys(table.referencingFields());
            let fkReferencesString = "";
            for (let ref of table.referencingFields()) {
                fkReferencesString += `, FOREIGN KEY (${ref.name}) REFERENCES ${ref.references.tableName()}(id)`;
            }
            let pkstring = `( ${pk.name} ${pk.type || "INT"} NOT NULL AUTO_INCREMENT`;
            let command = `CREATE TABLE IF NOT EXISTS ${table.tableName()} ${pkstring} ${fieldsString} ${fkReferencesString}
            , PRIMARY KEY ( ${pk.name} ) );`;
            // query database
            this.connection.query(command, (error, result) => {
                if (error) {
                    reject(error);
                } else {
                    resolve({result: result});
                }
            });
        });
    }

    newTableStrict(pk: { name: string, type: string }, table: IEntityModel) : Promise<{ result: any }> {
        return this.dropTable(table, true).then(res => this.newTable(pk, table));
    }

    dropTable(obj: IEntityModel, ifExists: boolean) : Promise<{ result: any }> {
        return new Promise<{ result: any }>((resolve, reject) => {
            let command = `DROP TABLE ${ ifExists ? "IF EXISTS" : "" }\`${obj.tableName()}\`;`;
            this.connection.query(command, (error, result) => {
                if (error) {
                    reject(error);
                } else {
                    resolve({result: result});
                }
            });
        });
    }
}
;