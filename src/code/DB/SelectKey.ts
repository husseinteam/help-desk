/// <reference path="../../../_all.d.ts" />
"use strict";

export class SelectKey {
    constructor(public name: string, public op1: string, public value: any, public op2: string = "AND") {
    }
    parametrify() {
        let sep = "'";
        if (parseInt(this.value)) {
            sep = "";
        }
        return ` \`${this.name}\` ${this.op1} ${sep}${this.value}${sep} ${this.op2}`;
    }
    trimify(): string {
        return this.parametrify().slice(0, -3);
    }
};