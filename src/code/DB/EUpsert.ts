/// <reference path="../../../_all.d.ts" />
"use strict";

export enum EUpsert {
    Insert,
    Update
}