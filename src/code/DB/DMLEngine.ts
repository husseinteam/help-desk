/// <reference path="../../../_all.d.ts" />

"use strict";
import {DBEngine} from "./DBEngine";
import {SelectKey} from "./SelectKey";
import {DBArgs} from "./DBArgs";
import {EUpsert} from "./EUpsert";
import {IModel} from "../Model/Base/IModel";
import {List} from "../Collections/List";

export class DMLEngine extends DBEngine {
    private connection: any;

    constructor(dbParams: DBArgs) {
        super();
        // get the client
        let mysql = require("mysql2");
        // create the connection
        this.connection = mysql.createConnection({
            host: dbParams.host,
            database: dbParams.database,
            user: dbParams.user,
            password: dbParams.password
        });
    }

    static reduceSelectKeys(selectKeys: [SelectKey]) {
        let constraintString = "";
        for (let skey of selectKeys) {
            constraintString += skey.parametrify();
        }
        constraintString = constraintString.slice(0, -4);
        return constraintString;
    }

    selectAll<TModel extends IModel<TModel>>(from: TModel): Promise<List<TModel>> {
        return this.select<TModel>(from, [new SelectKey("id", ">", 0)]);
    }

    select<TModel extends IModel<TModel>>(from: TModel,
                                          selectKeys: [SelectKey]): Promise<List<TModel>> {
        return new Promise<List<TModel>>((resolve, reject) => {
            let constraintString = DMLEngine.reduceSelectKeys(selectKeys);
            let values = [];
            for (let skey of selectKeys) {
                values.push(skey.value);
            }
            let command = `SELECT * FROM \`${from.tableName()}\` WHERE ${constraintString}`;
            // query database
            this.connection.query(command, values, (error, rows, fields) => {
                if (error) {
                    console.log(command);
                    reject(error);
                }
                let models = new List<TModel>();
                for (let row of rows) {
                    let mgen = from.generator();
                    models.add(mgen(row));
                }
                resolve(models);
            });
        });
    }

    insert<TModel extends IModel<TModel>>(entity: TModel): Promise<{ id: number, inserted: TModel }> {
        return new Promise<{ id: number, inserted: TModel }>((resolve, reject) => {
            let command = `INSERT INTO \`${entity.tableName()}\` SET ?`;
            let query = this.connection.query(command, entity, (error, result) => {
                if (error) {
                    console.log(command);
                    reject(error);
                }
                resolve({id: +result.insertId, inserted: entity});
            });
        });
    }

    update<TModel extends IModel<TModel>>(key: SelectKey,
                                          referenceEntity: TModel): Promise<{ changed: number, updated: TModel }> {
        return new Promise<{ changed: number, updated: TModel }>((resolve, reject) => {
            let values = [];
            for (let pair of referenceEntity.toKeyValuePairs()) {
                values.push(pair.value);
            }
            let constraintString = referenceEntity.parseSetPairs();
            let command = `UPDATE ${referenceEntity.tableName()} SET ${constraintString} WHERE ${key.trimify()}`;
            let query = this.connection.query(command, values, async(error, result) => {
                if (error) {
                    console.log(command);
                    reject(error);
                }
                let models = await this.select<TModel>(referenceEntity, [new SelectKey(key.name, "=", key.value)]);
                resolve({changed: result.changedRows, updated: models.get(0)});
            });
        });

    }

    upsert<TModel extends IModel<TModel>>(key: SelectKey,
                                          referenceEntity: TModel): Promise<{ result: number, upserted: TModel, type: EUpsert }> {
        return new Promise<{ result: number, upserted: TModel, type: EUpsert }>(async (resolve, reject) => {
            let models = await this.select(referenceEntity, [new SelectKey(key.name, "=", key.value)]);
            if (models.size() === 0) {
                this.insert<TModel>(referenceEntity).then(r => {
                    resolve({result: r.id, upserted: r.inserted, type: EUpsert.Insert});
                }).catch(reject);
            } else {
                this.update<TModel>(key, referenceEntity).then(r => {
                    resolve({result: r.changed, upserted: r.updated, type: EUpsert.Update});
                }).catch(reject);
            }
        });

    }

    delete<TModel extends IModel<TModel>>(table: TModel, selectKeys: [SelectKey]): Promise<{affected: number}> {
        return new Promise<{affected: number}>((resolve, reject) => {
            let constraintString = DMLEngine.reduceSelectKeys(selectKeys);
            let command = `DELETE FROM ${table.tableName()} WHERE ${constraintString}`;
            let query = this.connection.query(command, (error, result) => {
                if (error) {
                    console.log(command);
                    reject(error);
                }
                resolve({ affected: +result.affectedRows });
            });
        });
    }
}