/// <reference path="../../../_all.d.ts" />
"use strict";

export class DBArgs {
    public get host() {
        return this._host;
    }
    public get database() {
        return this._database;
    }
    public get user() {
        return this._user;
    }
    public get password() {
        return this._password;
    }
    constructor(private _host: string, private _database: string,  private _user: string, private _password: string) {}
    static get destekV1() {
        return new DBArgs("localhost", "destekdb", "destekadmin", "vartek_q1w2e3");
    }
}