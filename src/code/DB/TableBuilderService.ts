/// <reference path="../../../_all.d.ts" />
"use strict";
import {DDLEngine} from "./DDLEngine";
import {DBArgs} from "./DBArgs";
import {EmailModel} from "../Model/EmailModel";
import {CustomerModel} from "../Model/CustomerModel";
import {StaffModel} from "../Model/StaffModel";
import {ServiceCallModel} from "../Model/ServiceCallModel";
import {IEntityModel} from "../Model/Base/IEntityModel";
import {CompanyModel} from "../Model/CompanyModel";

export class TableBuilderService {
    constructor(dbParams: DBArgs) {
        this.ddle = new DDLEngine(dbParams);
    }
    private ddle: DDLEngine;
    async buildAll(hard: boolean) {
        let tables = [new EmailModel(), new CompanyModel(), new CustomerModel(), new StaffModel(), new ServiceCallModel()];
        for (let i = 0; i < tables.length; i++) {
            let table = tables[i];
            await this.build(hard, table);
        }
    }
    private build(hard: boolean, table: IEntityModel) {
        return hard ?
            this.ddle.newTableStrict({ name: "id", type: "INT" }, table) :
            this.ddle.newTable({ name: "id", type: "INT" }, table);
    }
}