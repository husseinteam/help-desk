/// <reference path="../../../_all.d.ts" />
"use strict";
const DBEngine_1 = require("./DBEngine");
class DDLEngine extends DBEngine_1.DBEngine {
    constructor(dbParams) {
        super();
        // get the client
        let mysql = require("mysql2");
        // create the connection
        this.connection = mysql.createConnection({
            host: dbParams.host,
            database: dbParams.database,
            user: dbParams.user,
            password: dbParams.password
        });
    }
    static reduceKeys(fields) {
        let fieldsString = "";
        for (let { name, type } of fields) {
            fieldsString += `, \`${name}\` ${type} NOT NULL`;
        }
        return fieldsString;
    }
    newTable(pk, table) {
        return new Promise((resolve, reject) => {
            let fieldsString = DDLEngine.reduceKeys(table.tableFields()) + DDLEngine.reduceKeys(table.referencingFields());
            let fkReferencesString = "";
            for (let ref of table.referencingFields()) {
                fkReferencesString += `, FOREIGN KEY (${ref.name}) REFERENCES ${ref.references.tableName()}(id)`;
            }
            let pkstring = `( ${pk.name} ${pk.type || "INT"} NOT NULL AUTO_INCREMENT`;
            let command = `CREATE TABLE IF NOT EXISTS ${table.tableName()} ${pkstring} ${fieldsString} ${fkReferencesString}
            , PRIMARY KEY ( ${pk.name} ) );`;
            // query database
            this.connection.query(command, (error, result) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve({ result: result });
                }
            });
        });
    }
    newTableStrict(pk, table) {
        return this.dropTable(table, true).then(res => this.newTable(pk, table));
    }
    dropTable(obj, ifExists) {
        return new Promise((resolve, reject) => {
            let command = `DROP TABLE ${ifExists ? "IF EXISTS" : ""}\`${obj.tableName()}\`;`;
            this.connection.query(command, (error, result) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve({ result: result });
                }
            });
        });
    }
}
exports.DDLEngine = DDLEngine;
;
