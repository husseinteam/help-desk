/// <reference path="../../../_all.d.ts" />
"use strict";
class SelectKey {
    constructor(name, op1, value, op2 = "AND") {
        this.name = name;
        this.op1 = op1;
        this.value = value;
        this.op2 = op2;
    }
    parametrify() {
        let sep = "'";
        if (parseInt(this.value)) {
            sep = "";
        }
        return ` \`${this.name}\` ${this.op1} ${sep}${this.value}${sep} ${this.op2}`;
    }
    trimify() {
        return this.parametrify().slice(0, -3);
    }
}
exports.SelectKey = SelectKey;
;
