/// <reference path="../../../_all.d.ts" />
"use strict";
var EUpsert;
(function (EUpsert) {
    EUpsert[EUpsert["Insert"] = 0] = "Insert";
    EUpsert[EUpsert["Update"] = 1] = "Update";
})(EUpsert = exports.EUpsert || (exports.EUpsert = {}));
