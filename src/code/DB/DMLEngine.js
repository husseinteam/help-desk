/// <reference path="../../../_all.d.ts" />
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const DBEngine_1 = require("./DBEngine");
const SelectKey_1 = require("./SelectKey");
const EUpsert_1 = require("./EUpsert");
const List_1 = require("../Collections/List");
class DMLEngine extends DBEngine_1.DBEngine {
    constructor(dbParams) {
        super();
        // get the client
        let mysql = require("mysql2");
        // create the connection
        this.connection = mysql.createConnection({
            host: dbParams.host,
            database: dbParams.database,
            user: dbParams.user,
            password: dbParams.password
        });
    }
    static reduceSelectKeys(selectKeys) {
        let constraintString = "";
        for (let skey of selectKeys) {
            constraintString += skey.parametrify();
        }
        constraintString = constraintString.slice(0, -4);
        return constraintString;
    }
    selectAll(from) {
        return this.select(from, [new SelectKey_1.SelectKey("id", ">", 0)]);
    }
    select(from, selectKeys) {
        return new Promise((resolve, reject) => {
            let constraintString = DMLEngine.reduceSelectKeys(selectKeys);
            let values = [];
            for (let skey of selectKeys) {
                values.push(skey.value);
            }
            let command = `SELECT * FROM \`${from.tableName()}\` WHERE ${constraintString}`;
            // query database
            this.connection.query(command, values, (error, rows, fields) => {
                if (error) {
                    console.log(command);
                    reject(error);
                }
                let models = new List_1.List();
                for (let row of rows) {
                    let mgen = from.generator();
                    models.add(mgen(row));
                }
                resolve(models);
            });
        });
    }
    insert(entity) {
        return new Promise((resolve, reject) => {
            let command = `INSERT INTO \`${entity.tableName()}\` SET ?`;
            let query = this.connection.query(command, entity, (error, result) => {
                if (error) {
                    console.log(command);
                    reject(error);
                }
                resolve({ id: +result.insertId, inserted: entity });
            });
        });
    }
    update(key, referenceEntity) {
        return new Promise((resolve, reject) => {
            let values = [];
            for (let pair of referenceEntity.toKeyValuePairs()) {
                values.push(pair.value);
            }
            let constraintString = referenceEntity.parseSetPairs();
            let command = `UPDATE ${referenceEntity.tableName()} SET ${constraintString} WHERE ${key.trimify()}`;
            let query = this.connection.query(command, values, (error, result) => __awaiter(this, void 0, void 0, function* () {
                if (error) {
                    console.log(command);
                    reject(error);
                }
                let models = yield this.select(referenceEntity, [new SelectKey_1.SelectKey(key.name, "=", key.value)]);
                resolve({ changed: result.changedRows, updated: models.get(0) });
            }));
        });
    }
    upsert(key, referenceEntity) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let models = yield this.select(referenceEntity, [new SelectKey_1.SelectKey(key.name, "=", key.value)]);
            if (models.size() === 0) {
                this.insert(referenceEntity).then(r => {
                    resolve({ result: r.id, upserted: r.inserted, type: EUpsert_1.EUpsert.Insert });
                }).catch(reject);
            }
            else {
                this.update(key, referenceEntity).then(r => {
                    resolve({ result: r.changed, upserted: r.updated, type: EUpsert_1.EUpsert.Update });
                }).catch(reject);
            }
        }));
    }
    delete(table, selectKeys) {
        return new Promise((resolve, reject) => {
            let constraintString = DMLEngine.reduceSelectKeys(selectKeys);
            let command = `DELETE FROM ${table.tableName()} WHERE ${constraintString}`;
            let query = this.connection.query(command, (error, result) => {
                if (error) {
                    console.log(command);
                    reject(error);
                }
                resolve({ affected: +result.affectedRows });
            });
        });
    }
}
exports.DMLEngine = DMLEngine;
