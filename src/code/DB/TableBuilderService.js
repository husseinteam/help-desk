/// <reference path="../../../_all.d.ts" />
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const DDLEngine_1 = require("./DDLEngine");
const EmailModel_1 = require("../Model/EmailModel");
const CustomerModel_1 = require("../Model/CustomerModel");
const StaffModel_1 = require("../Model/StaffModel");
const ServiceCallModel_1 = require("../Model/ServiceCallModel");
const CompanyModel_1 = require("../Model/CompanyModel");
class TableBuilderService {
    constructor(dbParams) {
        this.ddle = new DDLEngine_1.DDLEngine(dbParams);
    }
    buildAll(hard) {
        return __awaiter(this, void 0, void 0, function* () {
            let tables = [new EmailModel_1.EmailModel(), new CompanyModel_1.CompanyModel(), new CustomerModel_1.CustomerModel(), new StaffModel_1.StaffModel(), new ServiceCallModel_1.ServiceCallModel()];
            for (let i = 0; i < tables.length; i++) {
                let table = tables[i];
                yield this.build(hard, table);
            }
        });
    }
    build(hard, table) {
        return hard ?
            this.ddle.newTableStrict({ name: "id", type: "INT" }, table) :
            this.ddle.newTable({ name: "id", type: "INT" }, table);
    }
}
exports.TableBuilderService = TableBuilderService;
