/// <reference path="../../../_all.d.ts" />

"use strict";

export class List<T> {
    private items: Array<T>;

    constructor(arr?: [T]) {
        this.items = arr || [];
    }

    toArray() : Array<T> {
        return this.items;
    }

    size(): number {
        return this.items.length;
    }

    add(value: T): void {
        this.items.push(value);
    }

    get(index: number): T {
        return this.items[index];
    }

    *[Symbol.iterator](): IterableIterator<T> {
        for (let item of this.items) {
          yield item;
        }
    }
}