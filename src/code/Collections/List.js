/// <reference path="../../../_all.d.ts" />
"use strict";
class List {
    constructor(arr) {
        this.items = arr || [];
    }
    toArray() {
        return this.items;
    }
    size() {
        return this.items.length;
    }
    add(value) {
        this.items.push(value);
    }
    get(index) {
        return this.items[index];
    }
    *[Symbol.iterator]() {
        for (let item of this.items) {
            yield item;
        }
    }
}
exports.List = List;
