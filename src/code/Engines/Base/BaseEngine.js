/// <reference path="../../../../_all.d.ts" />
"use strict";
const DBArgs_1 = require("../../DB/DBArgs");
const DMLEngine_1 = require("../../DB/DMLEngine");
class BaseEngine {
    constructor() {
        this.dmle = new DMLEngine_1.DMLEngine(DBArgs_1.DBArgs.destekV1);
    }
}
exports.BaseEngine = BaseEngine;
