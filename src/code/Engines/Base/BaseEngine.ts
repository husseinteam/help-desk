/// <reference path="../../../../_all.d.ts" />
"use strict";
import {DBArgs} from "../../DB/DBArgs";
import {DMLEngine} from "../../DB/DMLEngine";

export class BaseEngine {

    protected dmle: DMLEngine;

    constructor() {
        this.dmle = new DMLEngine(DBArgs.destekV1);
    }

}