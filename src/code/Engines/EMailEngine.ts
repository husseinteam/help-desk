/// <reference path="../../../_all.d.ts" />
"use strict";
import {EmailModel} from "../Model/EmailModel";
import {DBArgs} from "../DB/DBArgs";
import {DMLEngine} from "../DB/DMLEngine";
import {TableBuilderService} from "../DB/TableBuilderService";
import {SelectKey} from "../DB/SelectKey";
import {List} from "../Collections/List";

let ews = require("ews-javascript-api");
let moment = require("moment");

export class EmailEngine {
    constructor(private email: string, private password: string) { }

    watch(count: number, callback: (args: { upserted: EmailModel }) => void) {
        this.list(count, async (r) => {
            let dmle = new DMLEngine(DBArgs.destekV1);
            dmle.upsert(new SelectKey("internetMessageId", "=", r.email.internetMessageId), new EmailModel(r.email))
                .then(rdml => callback({ upserted: rdml.upserted }));

        });
    }
    list(count: number, callback: (arg: {index: number, email: EmailModel, last: boolean}) => void) {
        let exch = new ews.ExchangeService(ews.ExchangeVersion.Exchange2007_SP1);
        ews.EwsLogging.DebugLogEnabled = false;

        exch.Credentials = new ews.ExchangeCredentials(this.email, this.password);
        exch.Url = new ews.Uri("https://ews.mail.eu-west-1.awsapps.com/EWS/Exchange.asmx");

        // folderId
        const userMailbox = new ews.Mailbox(this.email);
        const userFolderId = new ews.FolderId(ews.WellKnownFolderName.Inbox, userMailbox);

        // searchFilter
        const now = moment();
        const hours = now.hours();

        const startOfToday = new ews.DateTime(
            ews.DateTime.Now.TotalMilliSeconds - ews.TimeSpan.FromHours(hours).asMilliseconds()
        );
        //son 1 günkü mesajlar için:
        let afterTodayFilter = new ews.SearchFilter.IsGreaterThanOrEqualTo(
            ews.ItemSchema.DateTimeSent,
            startOfToday
        );

        // itemView
        let itemView = new ews.ItemView(count);

        let propertySet = new ews.PropertySet();
        propertySet.Add(ews.ItemSchema.Id);
        propertySet.Add(ews.ItemSchema.Subject);
        propertySet.Add(ews.ItemSchema.DateTimeCreated);
        propertySet.Add(ews.ItemSchema.HasAttachments);
        // propertySet.Add(ews.EmailMessageSchema.BccRecipients);
        //propertySet.Add(ews.EmailMessageSchema.CcRecipients);
        propertySet.Add(ews.EmailMessageSchema.ConversationIndex);
        propertySet.Add(ews.EmailMessageSchema.ConversationTopic);
        propertySet.Add(ews.EmailMessageSchema.From);
        propertySet.Add(ews.EmailMessageSchema.InternetMessageId);
        //propertySet.Add(ews.EmailMessageSchema.ReplyTo);
        propertySet.Add(ews.EmailMessageSchema.Sender);
        //propertySet.Add(ews.EmailMessageSchema.ToRecipients);

        itemView.propertySet = propertySet;
        let mails = new List<EmailModel>();
        exch.FindItems(userFolderId, undefined, itemView)
            .then((res: any) => {
                for (var i = 0; i < res.Items.length; i++) {
                    let item = res.Items[i];
                    ews.EmailMessage.Bind(exch, new ews.ItemId( item.propertyBag.properties.objects.Id.UniqueId)).then((mail: any) => {
                        let email = new EmailModel(mail);
                        mails.add(mail);
                        callback({index: mails.size() - 1, email: email, last: res.Items.length === mails.size()});
                    });
                }
            }, function(err: Error) {
                throw err;
            });
    }
}