/// <reference path="../../../_all.d.ts" />
"use strict";
import {ServiceCallModel} from "../Model/ServiceCallModel";
import {BaseEngine} from "./Base/BaseEngine";
import {List} from "../Collections/List";

export class ServiceCallEngine extends BaseEngine {

    constructor() {
        super();
    }

    public recentCalls(): Promise<List<ServiceCallModel>> {
        return this.dmle.selectAll(new ServiceCallModel());
    }

    static get singleton() {
        return new ServiceCallEngine();
    }
}
