/// <reference path="../../../_all.d.ts" />
"use strict";
const ServiceCallModel_1 = require("../Model/ServiceCallModel");
const BaseEngine_1 = require("./Base/BaseEngine");
class ServiceCallEngine extends BaseEngine_1.BaseEngine {
    constructor() {
        super();
    }
    recentCalls() {
        return this.dmle.selectAll(new ServiceCallModel_1.ServiceCallModel());
    }
    static get singleton() {
        return new ServiceCallEngine();
    }
}
exports.ServiceCallEngine = ServiceCallEngine;
