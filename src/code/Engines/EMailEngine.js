/// <reference path="../../../_all.d.ts" />
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const EmailModel_1 = require("../Model/EmailModel");
const DBArgs_1 = require("../DB/DBArgs");
const DMLEngine_1 = require("../DB/DMLEngine");
const SelectKey_1 = require("../DB/SelectKey");
const List_1 = require("../Collections/List");
let ews = require("ews-javascript-api");
let moment = require("moment");
class EmailEngine {
    constructor(email, password) {
        this.email = email;
        this.password = password;
    }
    watch(count, callback) {
        this.list(count, (r) => __awaiter(this, void 0, void 0, function* () {
            let dmle = new DMLEngine_1.DMLEngine(DBArgs_1.DBArgs.destekV1);
            dmle.upsert(new SelectKey_1.SelectKey("internetMessageId", "=", r.email.internetMessageId), new EmailModel_1.EmailModel(r.email))
                .then(rdml => callback({ upserted: rdml.upserted }));
        }));
    }
    list(count, callback) {
        let exch = new ews.ExchangeService(ews.ExchangeVersion.Exchange2007_SP1);
        ews.EwsLogging.DebugLogEnabled = false;
        exch.Credentials = new ews.ExchangeCredentials(this.email, this.password);
        exch.Url = new ews.Uri("https://ews.mail.eu-west-1.awsapps.com/EWS/Exchange.asmx");
        // folderId
        const userMailbox = new ews.Mailbox(this.email);
        const userFolderId = new ews.FolderId(ews.WellKnownFolderName.Inbox, userMailbox);
        // searchFilter
        const now = moment();
        const hours = now.hours();
        const startOfToday = new ews.DateTime(ews.DateTime.Now.TotalMilliSeconds - ews.TimeSpan.FromHours(hours).asMilliseconds());
        //son 1 günkü mesajlar için:
        let afterTodayFilter = new ews.SearchFilter.IsGreaterThanOrEqualTo(ews.ItemSchema.DateTimeSent, startOfToday);
        // itemView
        let itemView = new ews.ItemView(count);
        let propertySet = new ews.PropertySet();
        propertySet.Add(ews.ItemSchema.Id);
        propertySet.Add(ews.ItemSchema.Subject);
        propertySet.Add(ews.ItemSchema.DateTimeCreated);
        propertySet.Add(ews.ItemSchema.HasAttachments);
        // propertySet.Add(ews.EmailMessageSchema.BccRecipients);
        //propertySet.Add(ews.EmailMessageSchema.CcRecipients);
        propertySet.Add(ews.EmailMessageSchema.ConversationIndex);
        propertySet.Add(ews.EmailMessageSchema.ConversationTopic);
        propertySet.Add(ews.EmailMessageSchema.From);
        propertySet.Add(ews.EmailMessageSchema.InternetMessageId);
        //propertySet.Add(ews.EmailMessageSchema.ReplyTo);
        propertySet.Add(ews.EmailMessageSchema.Sender);
        //propertySet.Add(ews.EmailMessageSchema.ToRecipients);
        itemView.propertySet = propertySet;
        let mails = new List_1.List();
        exch.FindItems(userFolderId, undefined, itemView)
            .then((res) => {
            for (var i = 0; i < res.Items.length; i++) {
                let item = res.Items[i];
                ews.EmailMessage.Bind(exch, new ews.ItemId(item.propertyBag.properties.objects.Id.UniqueId)).then((mail) => {
                    let email = new EmailModel_1.EmailModel(mail);
                    mails.add(mail);
                    callback({ index: mails.size() - 1, email: email, last: res.Items.length === mails.size() });
                });
            }
        }, function (err) {
            throw err;
        });
    }
}
exports.EmailEngine = EmailEngine;
