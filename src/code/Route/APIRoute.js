/// <reference path="../../../_all.d.ts" />
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const BaseRoute_1 = require("./BaseRoute");
const HTTPMethod_1 = require("./HTTPMethod");
class APIRoute extends BaseRoute_1.BaseRoute {
    constructor(params) {
        super(params);
    }
    setApiRoutes() {
        for (let api of this.apiRoutes()) {
            switch (api.method) {
                case HTTPMethod_1.HTTPMethod.GET:
                    this.routeArgs.app.get(api.route, (req, res, next) => __awaiter(this, void 0, void 0, function* () {
                        res.json({ data: yield api.dataGenerator(req.params) });
                    }));
                    break;
                case HTTPMethod_1.HTTPMethod.POST:
                    this.routeArgs.app.get(api.route, (req, res, next) => __awaiter(this, void 0, void 0, function* () {
                        res.json({ data: yield api.dataGenerator(req.params) });
                    }));
                    break;
                case HTTPMethod_1.HTTPMethod.PUT:
                    this.routeArgs.app.put(api.route, (req, res, next) => __awaiter(this, void 0, void 0, function* () {
                        res.json({ data: yield api.dataGenerator(req.params) });
                    }));
                    break;
                case HTTPMethod_1.HTTPMethod.DELETE:
                    this.routeArgs.app.delete(api.route, (req, res, next) => __awaiter(this, void 0, void 0, function* () {
                        res.json({ data: yield api.dataGenerator(req.params) });
                    }));
                    break;
            }
        }
    }
}
exports.APIRoute = APIRoute;
