/// <reference path="../../../_all.d.ts" />
"use strict";
class RouteArgs {
    constructor(_app, _route, _params) {
        this._app = _app;
        this._route = _route;
        this._params = _params;
    }
    get app() {
        return this._app;
    }
    get route() {
        return this._route;
    }
    get params() {
        return this._params;
    }
}
exports.RouteArgs = RouteArgs;
