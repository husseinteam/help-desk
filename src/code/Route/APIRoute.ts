/// <reference path="../../../_all.d.ts" />
"use strict";

import {BaseRoute} from "./BaseRoute";
import {RouteArgs} from "./RouteArgs";
import {List} from "../Collections/List";
import {HTTPMethod} from "./HTTPMethod";
import {IAPIRoute} from "./IAPIRoute";

export abstract class APIRoute<TEntity> extends BaseRoute implements IAPIRoute<TEntity> {

    public abstract apiRoutes(): List<{ route: string, method: HTTPMethod, dataGenerator: (params: any) => Promise<List<TEntity>> }>;

    constructor(params: RouteArgs) {
        super(params);
    }

    public setApiRoutes() {
        for (let api of this.apiRoutes()) {
            switch (api.method) {
                case HTTPMethod.GET:
                    this.routeArgs.app.get(api.route, async (req, res, next) => {
                        res.json({ data: await api.dataGenerator(req.params) });
                    });
                    break;
                case HTTPMethod.POST:
                    this.routeArgs.app.get(api.route, async (req, res, next) => {
                        res.json({ data: await api.dataGenerator(req.params) });
                    });
                    break;
                case HTTPMethod.PUT:
                    this.routeArgs.app.put(api.route, async (req, res, next) => {
                        res.json({ data: await api.dataGenerator(req.params) });
                    });
                    break;
                case HTTPMethod.DELETE:
                    this.routeArgs.app.delete(api.route, async (req, res, next) => {
                        res.json({ data: await api.dataGenerator(req.params) });
                    });
                    break;
            }
        }
    }
}