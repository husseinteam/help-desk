/// <reference path="../../../_all.d.ts" />
"use strict";
const BaseRoute_1 = require("./BaseRoute");
class DefaultRoute extends BaseRoute_1.BaseRoute {
    constructor(params, defaultRoute) {
        super(params);
        this.defaultRoute = defaultRoute;
    }
    setDefaultRoute() {
        this.routeArgs.app.get(this.routeArgs.route, (req, res, next) => {
            res.render(this.defaultRoute, this.routeArgs.params);
        });
    }
}
exports.DefaultRoute = DefaultRoute;
