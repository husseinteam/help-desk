/// <reference path="../../../_all.d.ts" />
"use strict";

import { Request, Response, NextFunction } from "express";

export interface IBaseRoute {
    setDefaultRoute();
}