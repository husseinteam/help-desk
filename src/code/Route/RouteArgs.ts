/// <reference path="../../../_all.d.ts" />
"use strict";

import * as express from "express";

export class RouteArgs {
    constructor(private _app: express.Application, private _route?: string, private _params?: any) { }

    public get app() {
        return this._app;
    }
    public get route() {
        return this._route;
    }
    public get params() {
        return this._params;
    }
}