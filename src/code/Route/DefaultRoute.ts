/// <reference path="../../../_all.d.ts" />
"use strict";

import {BaseRoute} from "./BaseRoute";
import {IBaseRoute} from "./IBaseRoute";
import {RouteArgs} from "./RouteArgs";
import {List} from "../Collections/List";
import {HTTPMethod} from "./HTTPMethod";

export class DefaultRoute extends BaseRoute implements IBaseRoute {

    private defaultRoute: String;

    constructor(params: RouteArgs, defaultRoute: String) {
        super(params);
        this.defaultRoute = defaultRoute;
    }

    public setDefaultRoute() {
        this.routeArgs.app.get(this.routeArgs.route, (req, res, next) => {
            res.render(this.defaultRoute, this.routeArgs.params);
        });
    }
}