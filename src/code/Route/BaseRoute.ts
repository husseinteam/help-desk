/// <reference path="../../../_all.d.ts" />
"use strict";

import {RouteArgs} from "./RouteArgs";
export class BaseRoute {
    constructor(args: RouteArgs) {
        this.routeArgs = args;
    }
    protected routeArgs: RouteArgs;
}