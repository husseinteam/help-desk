/// <reference path="../../../_all.d.ts" />
"use strict";

import { Request, Response, NextFunction } from "express";
import {HTTPMethod} from "./HTTPMethod";
import {List} from "../Collections/List";

export interface IAPIRoute<TEntity> {

    apiRoutes(): List<{ route: string, method: HTTPMethod, dataGenerator: (params: any) => Promise<List<TEntity>> }>;
    setApiRoutes<T>();

}