/// <reference path="../../../_all.d.ts" />
"use strict";

export enum HTTPMethod {
    GET,
    POST,
    PUT,
    DELETE
}