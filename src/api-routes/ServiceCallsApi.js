/// <reference path="../../_all.d.ts" />
"use strict";
const APIRoute_1 = require("../code/Route/APIRoute");
const List_1 = require("../code/Collections/List");
const HTTPMethod_1 = require("../code/Route/HTTPMethod");
const ServiceCallEngine_1 = require("../code/Engines/ServiceCallEngine");
class ServiceCallsApi extends APIRoute_1.APIRoute {
    apiRoutes() {
        return new List_1.List([
            { route: "recent-calls", method: HTTPMethod_1.HTTPMethod.GET, dataGenerator: (params) => { return ServiceCallEngine_1.ServiceCallEngine.singleton.recentCalls(); } }
        ]);
    }
    constructor(params) {
        super(params);
    }
}
exports.ServiceCallsApi = ServiceCallsApi;
