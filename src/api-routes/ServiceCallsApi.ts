/// <reference path="../../_all.d.ts" />
"use strict";
import {ServiceCallModel} from "../code/Model/ServiceCallModel";
import {APIRoute} from "../code/Route/APIRoute";
import {List} from "../code/Collections/List";
import {HTTPMethod} from "../code/Route/HTTPMethod";
import {ServiceCallEngine} from "../code/Engines/ServiceCallEngine";
import {RouteArgs} from "../code/Route/RouteArgs";


export class ServiceCallsApi extends APIRoute<ServiceCallModel> {

    public apiRoutes(): List<{ route: string, method: HTTPMethod, dataGenerator: (params: any) => Promise<List<ServiceCallModel>> }> {
        return new List<{ route: string, method: HTTPMethod, dataGenerator: (params: any) => Promise<List<ServiceCallModel>> }>([
            { route: "recent-calls", method: HTTPMethod.GET, dataGenerator: (params: any) => { return ServiceCallEngine.singleton.recentCalls(); } }
        ]);
    }

    constructor(params: RouteArgs) {
        super(params);
    }

}