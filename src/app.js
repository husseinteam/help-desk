/// <reference path="../_all.d.ts" />
"use strict";
const bodyParser = require("body-parser");
const express = require("express");
const logger = require("morgan");
const RouteArgs_1 = require("./code/Route/RouteArgs");
const ServiceCallsApi_1 = require("./api-routes/ServiceCallsApi");
/**
 * The server.
 *
 * @class Server
 */
class Server {
    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
     */
    static bootstrap() {
        return new Server();
    }
    config() {
        Server.setApiRoutes(this.app);
        this.middleware();
    }
    middleware() {
        this.app.use(logger("dev"));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }
    static setApiRoutes(app) {
        [
            new ServiceCallsApi_1.ServiceCallsApi(new RouteArgs_1.RouteArgs(app))
        ].forEach(api => {
            api.setApiRoutes();
        });
    }
    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    constructor() {
        //create expressjs application
        this.app = express();
        //configure application
        this.config();
    }
}
Server.bootstrap();
