/// <reference path="../_all.d.ts" />
"use strict";

import * as bodyParser from "body-parser";
import * as express from "express";
import * as logger from "morgan";
import * as path from "path";
import {RouteArgs} from "./code/Route/RouteArgs";
import {ServiceCallsApi} from "./api-routes/ServiceCallsApi";

/**
 * The server.
 *
 * @class Server
 */
class Server {

    public app: express.Application;

    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
     */
    public static bootstrap(): Server {
        return new Server();
    }

    private config() {
        Server.setApiRoutes(this.app);
        this.middleware();

    }

    private middleware() {
        this.app.use(logger("dev"));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));
    }

    private static setApiRoutes(app: express.Application) {
        [
            new ServiceCallsApi(new RouteArgs(app))
        ].forEach(api => {
            api.setApiRoutes();
        });
    }

    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    constructor() {
        //create expressjs application
        this.app = express();

        //configure application
        this.config();
    }
}

Server.bootstrap();