module.exports = function(grunt) {
    "use strict";

    grunt.initConfig({
        ts: {
            app: {
                files: [{
                    src: ["src/\*\*/\*.ts", "!typings/\*\*/\*.ts", "!.baseDir.ts", "!_all.d.ts"],
                }],
                tsconfig: 'tsconfig.json'
            }
        },
        tslint: {
            options: {
                configuration: "tslint.json"
            },
            files: {
                src: ["src/\*\*/\*.ts", "!typings/\*\*/\*.ts"]
            }
        },
        exec: {
            test: "mocha src/test/*.js --reporter spec",
			start: "concurrently \"tsc -p src -w\" \"node src/app.js\" \"lite-server -c=bs-config.destek.json\"",
        },
        connect: {
            server: {
                options: {
                    port: 8090,
                    base: './'
                }
            }
        },
        open: {
            dev: {
                path: 'http://localhost:8090/service-calls'
            }
        },
        watch: {
            ts: {
                files: ["js/src/\*\*/\*.ts", "src/\*\*/\*.ts"],
                tasks: ["ts", "tslint"]
            }
        }
    });

    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks("grunt-tslint");
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks('grunt-open');

    grunt.registerTask("default", [
        "ts",
        "tslint",
        "exec:test"
    ]);

    grunt.registerTask("web", [
        "ts",
        "tslint",
        //"exec:test",
        "exec:start"
        //"connect",
        //"open",
        //"watch"
    ]);

};